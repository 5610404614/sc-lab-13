package First;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws Exception {
		 List<String> q = Collections.synchronizedList(new LinkedList<String>());
		 PrepareProduction prepare = new PrepareProduction(q);
		 DoProduction doProduction = new DoProduction(q);
	      Thread p1 = new Thread(prepare);
	      Thread t1 = new Thread(doProduction);
	      Thread t2 = new Thread(doProduction);
	      Thread t3 = new Thread(doProduction);
	      Thread t4 = new Thread(doProduction);
	      Thread t5 = new Thread(doProduction);
	      p1.start();
	      t1.start();
	      t2.start();
	      t3.start();
	      t4.start();
	      t5.start();
	      t1.join();
	      t2.join();
	      t3.join();
	      t4.join();
	      t5.join();
	      System.out.println("done");
	   }
}
