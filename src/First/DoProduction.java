package First;

import java.util.List;

public class DoProduction implements Runnable {
	   private List<String> queue;
	   public DoProduction(List<String> q) { 
		   queue = q; 
	   }
	 
	   public void run() {
	      try {
	         String value = queue.remove(0);
	         while (!value.equals("*")) {
	            System.out.println(Thread.currentThread().getName()
	               +": " + value );   
	            value = queue.remove(0);
	        }
	      }
	      catch (Exception e) {
	         System.out.println(Thread.currentThread().getName() 
	             + " " + e.getMessage());
	      }
	   }
	}