package Second;
public class Consumer implements Runnable {
	private Queue queue;
	private final int TIMES = 100;

	public Consumer(Queue q) {
		this.queue = q;
	}

	@Override
	public void run() {
		try {
			for (int i =0; i< TIMES;i++){
				System.out.println(queue.dequeue());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
