package Second;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue extends ArrayList<String> {
	private Lock queue;
	private Condition condition;
	
	public Queue(){
		queue = new ReentrantLock();
		condition = queue.newCondition();
	}
	public void enqueue(String s) throws InterruptedException{
		queue.lock();
		try{
			while (this.size() >= 10){
				condition.await();
			}
			add(s);
			condition.signalAll();
		}finally{
			queue.unlock();
		}
		
	}
	public String dequeue() throws InterruptedException{
		String out;
		queue.lock();
		try{
			while (this.isEmpty()){
				condition.await();
			}
			out = remove(0);
			condition.signalAll();
		}finally{
			queue.unlock();
		}
		return out;
	}

}
