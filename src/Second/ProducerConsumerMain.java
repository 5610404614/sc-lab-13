package Second;
public class ProducerConsumerMain {

	public static void main(String[] args) {
		
		Queue q = new Queue();
		
		Producer pd = new Producer(q);
		Consumer c = new Consumer(q);
		
		Thread pdThread = new Thread(pd);
		Thread cThread = new Thread(c);
		
		pdThread.start();
		cThread.start();
		

	}

}
